package helpers

import (
	"encoding/json"
	"os"

	"project.model/models"
)

func LoadConfiguration(filepath string) *models.AppConfigModel {
	var res models.AppConfigModel

	configFile, err := os.Open(filepath)

	defer configFile.Close()

	if err != nil {
		return nil
	}

	jsonParser := json.NewDecoder(configFile)
	err = jsonParser.Decode(&res)
	if err != nil {
		return nil
	}
	return &res
}
