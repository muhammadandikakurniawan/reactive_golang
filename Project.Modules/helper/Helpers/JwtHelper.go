package helpers

import (
	"fmt"

	"github.com/dgrijalva/jwt-go"
	"project.model/models"
)

var JWT_SIGNING_METHOD = jwt.SigningMethodHS256

func CreateJwtToken(param models.UserLoginModel, secret string, exp int64) (string, error) {

	token := jwt.NewWithClaims(JWT_SIGNING_METHOD, jwt.MapClaims{
		"user_name":  param.UserName,
		"email":      param.Email,
		"user_id":    param.ID,
		"exp":        exp,
		"authorized": true,
	})

	tokenString, err := token.SignedString([]byte(secret))

	return tokenString, err
}

func VerifyToken(tokenStr, signatureKey string) (jwt.MapClaims, error) {

	tokenRes, tokenErr := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("%s", "Token is not valid")
		} else if method != JWT_SIGNING_METHOD {
			return nil, fmt.Errorf("%s", "Token is not valid")
		}

		return []byte(signatureKey), nil
	})

	if tokenErr != nil {
		return nil, tokenErr
	}

	claims, ok := tokenRes.Claims.(jwt.MapClaims)

	if !ok || !tokenRes.Valid {
		return nil, fmt.Errorf("%s", "Token is not valid")
	}

	return claims, nil

}
