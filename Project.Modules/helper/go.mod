module project.helper

go 1.15

replace project.model => ./../model

replace project.repository => ./../repository

require (
	project.model v0.0.1
	project.repository v0.0.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
)
