package entities

import "time"

type BaseEntity struct {
	ID          string    `gorm:"primaryKey"`
	CreatedDate time.Time `gorm:"column:CreatedDate"`
	IsDeleted   bool      `gorm:"column:IsDeleted"`
}
