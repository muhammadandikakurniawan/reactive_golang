package entities

type PostFile struct {
	BaseEntity BaseEntity `gorm:"embedded"`
	PostID     string     `gorm:"column:PostID"`
	FileType   string     `gorm:"column:FileType"`
	FileSize   int64      `gorm:"column:FileSize"`
	FileName   string     `gorm:"column:FileName"`
}
