package entities

type User struct {
	BaseEntity BaseEntity `gorm:"embedded"`
	UserName   string     `gorm:"column:UserName"`
	FullName   string     `gorm:"column:FullName"`
	Email      string     `gorm:"column:Email"`
	Password   string     `gorm:"column:Password"`
	Phone      string     `gorm:"column:Phone"`
	RoleCode   string     `gorm:"column:RoleCode"`
}
