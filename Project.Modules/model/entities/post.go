package entities

type Post struct {
	BaseEntity BaseEntity `gorm:"embedded"`
	UserID     string     `gorm:"column:UserID"`
	Title      string     `gorm:"column:Title"`
	Content    string     `gorm:"column:Content"`
}
