package models

type DataUserLoginModel struct {
	UserName string `json:"UserName"`
	Email    string `json:"Email"`
	ID       string `json:"ID"`
}

type AppConfigModel struct {
	DataBaseConfig struct {
		RestGoProject struct {
			Username string `json:"Username"`
			Password string `json:"Password"`
			DbHost   string `json:"DbHost"`
			DbPort   string `json:"DbPort"`
			DbName   string `json:"DbName"`
		} `json:"RestGoProject"`
	} `json:"DataBaseConfig"`
	JwtConfig struct {
		Issuer    string `json:"Issuer"`
		SecretKey string `json:"SecretKey"`
	} `json:"JwtConfig"`
	ServerConfig struct {
		Host string `json:"Host"`
		Port string `json:"Port"`
	} `json:"ServerConfig"`
	PostFileDirPath string `json:"PostFileDirPath"`
}

type ServiceResultModel struct {
	StatusCode    string      `json:"StatusCode"`
	StatusMessage string      `json:"StatusMessage"`
	Value         interface{} `json:"Value"`
}

func (model *ServiceResultModel) SetError() {
	model.StatusCode = "500"
	model.StatusMessage = "Internal Error"
	model.Value = nil
}

type RepositoryResultModel struct {
	IsSuccess    bool   `json:"IsSuccess"`
	ErrorMessage string `json:"ErrorMessage"`
}

type PaginationResultModel struct {
	StatusCode    string        `json:"StatusCode"`
	StatusMessage string        `json:"StatusMessage"`
	Values        []interface{} `json:"Values"`
}
