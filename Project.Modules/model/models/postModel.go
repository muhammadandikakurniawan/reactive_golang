package models

import "mime/multipart"

type AddPostParam struct {
	Title   string                  `schema:"Title,required"`
	Content string                  `schema:"Content,required"`
	Files   []*multipart.FileHeader `schema:"Files"`
}
