package models

import "time"

type UserModel struct {
	ID          string    `json:"ID"`
	CreatedDate time.Time `json:"CreatedDate"`
	IsDeleted   bool      `json:"IsDeleted"`
	UserName    string    `json:"UserName" binding:"required"`
	Email       string    `json:"Email" binding:"required"`
	Password    string    `json:"Password" binding:"required"`
	Phone       string    `json:"Phone"`
	FullName    string    `json:"FullName" binding:"required,min=10"`
	RoleCode    string    `json:"RoleCode" binding:"required,min=10"`
}

type UserLoginParamModel struct {
	Email    string `json:"Email" binding:"required"`
	Password string `json:"Password" binding:"required"`
}

type UserLoginResultModel struct {
	AccessToken     string `json:"AccessToken" binding:"required"`
	TokenExpiryDate string `json:"TokenExpiryDate" binding:"required"`
}

type UserLoginModel struct {
	ID       string `json:"ID" binding:"required"`
	UserName string `json:"UserName" binding:"required"`
	Email    string `json:"Email" binding:"required"`
}

type UserRegisterModel struct {
	UserName string `json:"UserName" binding:"required"`
	Email    string `json:"Email" binding:"required"`
	Password string `json:"Password" binding:"required"`
	Phone    string `json:"Phone"`
	FullName string `json:"FullName" binding:"required,min=10"`
}
