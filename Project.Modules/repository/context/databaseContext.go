package context

import (
	"fmt"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func InitDatabaseContext(db_username, db_password, db_host, db_port, db_name string) *DatabaseContext {

	obj := configureDatabaseContext(db_username, db_password, db_host, db_port, db_name)

	return obj
}

type DatabaseContext struct {
	Db *gorm.DB

	User     *gorm.DB
	Post     *gorm.DB
	PostFile *gorm.DB
}

func configureDatabaseContext(db_username, db_password, db_host, db_port, db_name string) (result *DatabaseContext) {
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=true&loc=Local", db_username, db_password, db_host, db_port, db_name)
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		return nil
	}
	result = &DatabaseContext{}
	result.Db = db
	result.DbSet()
	return result
}

func (obj *DatabaseContext) DbSet() {
	obj.User = obj.Db.Table("User")
	obj.Post = obj.Db.Table("Post")
	obj.PostFile = obj.Db.Table("PostFile")
}
