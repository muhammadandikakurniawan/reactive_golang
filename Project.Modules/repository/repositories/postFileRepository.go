package repositories

import (
	"fmt"
	"strings"
	"time"

	"github.com/gofrs/uuid"
	"gorm.io/gorm"
	"project.model/entities"
	"project.repository/context"
)

func InitPostFileRepository(dbContext *context.DatabaseContext) *PostFileRepository {
	return &PostFileRepository{
		dbContext: dbContext,
	}
}

type PostFileRepository struct {
	dbContext *context.DatabaseContext
}

func (repository *PostFileRepository) Create(model *entities.Post) error {

	uuidRes, err := uuid.NewV4()

	if err != nil {
		return err
	}

	model.BaseEntity.ID = uuidRes.String()
	model.BaseEntity.CreatedDate = time.Now()
	model.BaseEntity.IsDeleted = false
	err = repository.dbContext.PostFile.Create(model).Error

	if err != nil {
		return err
	}

	return nil
}

func (repository *PostFileRepository) CreateRangeTransaction(db *gorm.DB, models []*entities.PostFile) error {

	commandValues := ""

	for i, _ := range models {
		uuidRes, err := uuid.NewV4()

		if err != nil {
			return err
		}
		models[i].BaseEntity.ID = uuidRes.String()
		models[i].BaseEntity.CreatedDate = time.Now()
		models[i].BaseEntity.IsDeleted = false

		data := models[i]

		commandValues += fmt.Sprintf("('%v', '%v', %v, '%v', '%v', %v, '%v')",
			data.BaseEntity.ID,
			strings.Split(data.BaseEntity.CreatedDate.String(), ".")[0],
			data.BaseEntity.IsDeleted,
			data.PostID,
			data.FileType,
			data.FileSize,
			data.FileName,
		)

		if i < len(models)-1 {
			commandValues += ","
		}
	}

	command := fmt.Sprintf("INSERT INTO PostFile (`ID`, `CreatedDate`, `IsDeleted`, `PostID`, `FileType`, `FileSize`, `FileName`) VALUES %s;", commandValues)
	fmt.Println("")
	fmt.Println(command)
	err := db.Exec(command).Error
	if err != nil {
		return err
	}

	return nil
}

func (repository *PostFileRepository) Update(model *entities.User) error {

	err := repository.dbContext.PostFile.Updates(model).Where("ID = ?", model.BaseEntity.ID).Error

	if err != nil {
		return err
	}

	return nil
}

func (repository *PostFileRepository) GetByID(id string, model *entities.User) error {

	err := repository.dbContext.PostFile.Where("ID = ?", id).First(model).Error

	return err
}
