package repositories

import (
	"time"

	"github.com/gofrs/uuid"
	"project.model/entities"
	"project.repository/context"
)

func InitPostRepository(dbContext *context.DatabaseContext) *PostRepository {
	return &PostRepository{
		dbContext: dbContext,
	}
}

type PostRepository struct {
	dbContext *context.DatabaseContext
}

func (repository *PostRepository) Create(model *entities.Post) error {

	uuidRes, err := uuid.NewV4()

	if err != nil {
		return err
	}

	model.BaseEntity.ID = uuidRes.String()
	model.BaseEntity.CreatedDate = time.Now()
	model.BaseEntity.IsDeleted = false
	err = repository.dbContext.Post.Create(model).Error

	if err != nil {
		return err
	}

	return nil
}

func (repository *PostRepository) Update(model *entities.User) error {

	err := repository.dbContext.Post.Updates(model).Where("ID = ?", model.BaseEntity.ID).Error

	if err != nil {
		return err
	}

	return nil
}

func (repository *PostRepository) GetByID(id string, model *entities.User) error {

	err := repository.dbContext.Post.Where("ID = ?", id).First(model).Error

	return err
}
