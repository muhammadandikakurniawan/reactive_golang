package repositories

import (
	"time"

	"github.com/gofrs/uuid"
	"project.model/entities"
	"project.repository/context"
)

func InitUserRepository(dbContext *context.DatabaseContext) *UserRepository {
	return &UserRepository{
		dbContext: dbContext,
	}
}

type UserRepository struct {
	dbContext *context.DatabaseContext
}

func (repository *UserRepository) Create(model *entities.User) error {

	uuidRes, err := uuid.NewV4()

	if err != nil {
		return err
	}

	model.BaseEntity.ID = uuidRes.String()
	model.BaseEntity.CreatedDate = time.Now()
	model.BaseEntity.IsDeleted = false
	err = repository.dbContext.User.Create(model).Error

	if err != nil {
		return err
	}

	return nil
}

func (repository *UserRepository) Update(model *entities.User) error {

	err := repository.dbContext.User.Updates(model).Where("ID = ?", model.BaseEntity.ID).Error

	if err != nil {
		return err
	}

	return nil
}

func (repository *UserRepository) GetByID(id string, model *entities.User) error {

	err := repository.dbContext.User.Where("ID = ?", id).First(model).Error

	return err
}

func (repository *UserRepository) GetByEmail(email string, model *entities.User) error {

	err := repository.dbContext.User.Where("Email = ?", email).First(model).Error

	return err
}
