package repositories

import "project.repository/context"

func InitBaseRepository(dbContext *context.DatabaseContext) *BaseRepository {
	return &BaseRepository{
		dbContext: dbContext,
	}
}

type BaseRepository struct {
	dbContext *context.DatabaseContext
}
