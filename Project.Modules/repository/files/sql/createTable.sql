use RestGoProject;

create table `Role`(
	ID varchar(100) primary key not null,
    CreatedDate timestamp not null,
    IsDeleted bool not null,
    `Code` varchar(100) not null unique,
    `Name` varchar(100) not null
);

create table `User`(
	ID varchar(100) primary key not null,
    CreatedDate timestamp not null,
    IsDeleted bool not null,
    UserName varchar(100) not null,
    Email varchar(100) not null,
    Phone varchar(20),
    RoleCode varchar(100),
    foreign key (RoleCode) references `Role`(Code)
);