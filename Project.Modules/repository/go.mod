module project.repository

go 1.16

replace project.model => ./../model

replace project.repository => ./../repository

require (
	github.com/gofrs/uuid v4.0.0+incompatible
	github.com/nu7hatch/gouuid v0.0.0-20131221200532-179d4d0c4d8d // indirect
	gorm.io/driver/mysql v1.0.5
	gorm.io/gorm v1.21.3
	project.model v0.0.1
)
