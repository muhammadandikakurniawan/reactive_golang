module project.service

go 1.16

replace project.model => ./../model

replace project.repository => ./../repository

replace project.helper => ./../helper

require (
	github.com/gorilla/context v1.1.1
	github.com/nu7hatch/gouuid v0.0.0-20131221200532-179d4d0c4d8d
	golang.org/x/crypto v0.0.0-20210317152858-513c2a44f670
	project.helper v0.0.1
	project.model v0.0.1
	project.repository v0.0.1
)
