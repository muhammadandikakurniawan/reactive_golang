package services

import (
	"time"

	"golang.org/x/crypto/bcrypt"
	"project.helper/helpers"
	"project.model/entities"
	"project.model/models"
	"project.repository/repositories"
)

func InitUserService(userRepository *repositories.UserRepository, appConfig *models.AppConfigModel) *UserService {
	return &UserService{
		userRepository: userRepository,
		appConfig:      appConfig,
	}
}

type IUserService interface {
	Register(param models.UserRegisterModel) *models.ServiceResultModel
	Login(param models.UserLoginModel) *models.ServiceResultModel
}

type UserService struct {
	userRepository *repositories.UserRepository
	appConfig      *models.AppConfigModel
}

func (service *UserService) Register(param models.UserRegisterModel) *models.ServiceResultModel {
	result := &models.ServiceResultModel{}
	result.StatusCode = "00"
	result.StatusMessage = "Register Successfull"

	passwordByte := []byte(param.Password)
	passwordHash, err := bcrypt.GenerateFromPassword(passwordByte, bcrypt.DefaultCost)

	if err != nil {
		result.SetError()
		return result
	}

	dataInsert := &entities.User{
		UserName: param.UserName,
		Email:    param.Email,
		Phone:    param.Phone,
		Password: string(passwordHash),
		FullName: param.FullName,
		RoleCode: "USER",
	}

	err = service.userRepository.Create(dataInsert)

	if err != nil {
		result.SetError()
		return result
	}

	return result
}

func (service *UserService) Login(param models.UserLoginParamModel) *models.ServiceResultModel {
	result := &models.ServiceResultModel{
		StatusCode:    "00",
		StatusMessage: "success access Login service",
	}

	var dataUser entities.User

	err := service.userRepository.GetByEmail(param.Email, &dataUser)

	if err != nil {
		result.StatusCode = "40"
		result.StatusMessage = "User not found"
		return result
	}

	err = bcrypt.CompareHashAndPassword([]byte(dataUser.Password), []byte(param.Password))
	if err != nil {
		result.StatusCode = "41"
		result.StatusMessage = "Password wrong"
		return result
	}

	dataClaims := models.UserLoginModel{
		ID:       dataUser.BaseEntity.ID,
		Email:    dataUser.Email,
		UserName: dataUser.UserName,
	}

	exp := time.Now().Add(time.Minute * 60)
	tokenStr, tokenError := helpers.CreateJwtToken(dataClaims, service.appConfig.JwtConfig.SecretKey, exp.Unix())
	if tokenError != nil {
		result.StatusCode = "42"
		result.StatusMessage = "Create Token failed"
		return result
	}
	result.Value = &models.UserLoginResultModel{
		AccessToken:     tokenStr,
		TokenExpiryDate: exp.Format("Jan 2, 2006 at 3:04pm (MST)"),
	}

	return result
}

// func (service *UserService) Login(param models.UserLoginModel, r *http.Request) *models.ServiceResultModel {
// 	result := &models.ServiceResultModel{}
// 	result.StatusCode = "00"
// 	result.StatusMessage = "Register Successfull"

// 	var dataUserLogin models.DataUserLoginModel

// 	dataUserLoginsCtx := context.Get(r, "userlogin")

// 	bt, _ := json.Marshal(dataUserLoginsCtx)
// 	json.Unmarshal(bt, &dataUserLogin)

// 	fmt.Print(dataUserLogin)

// 	// passwordByte := []byte(param.Password)
// 	// passwordHash, err := bcrypt.GenerateFromPassword(passwordByte, bcrypt.DefaultCost)
// 	// errComparePass := bcrypt.CompareHashAndPassword([]byte(dataUser.Password), []byte(p.Password))
// 	// fmt.

// 	return result
// }

func (service *UserService) GetUserById(id string) (*models.UserModel, error) {
	var resultRaw entities.User
	err := service.userRepository.GetByID(id, &resultRaw)

	if err != nil {
		return nil, err
	}

	data := &models.UserModel{
		ID:          resultRaw.BaseEntity.ID,
		CreatedDate: resultRaw.BaseEntity.CreatedDate,
		IsDeleted:   resultRaw.BaseEntity.IsDeleted,
		UserName:    resultRaw.UserName,
		Password:    resultRaw.Password,
		FullName:    resultRaw.FullName,
		Email:       resultRaw.Email,
		RoleCode:    resultRaw.RoleCode,
	}

	return data, nil
}
