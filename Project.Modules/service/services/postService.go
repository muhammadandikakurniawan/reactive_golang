package services

import (
	"fmt"
	"io"
	"os"
	"strings"
	"time"

	uuid "github.com/nu7hatch/gouuid"
	"project.model/entities"
	"project.model/models"
	"project.repository/context"
	"project.repository/repositories"
)

func InitPostService(
	appConfig *models.AppConfigModel,
	postRepository *repositories.PostRepository,
	postFileRepository *repositories.PostFileRepository,
	dbContext *context.DatabaseContext,
) *PostService {

	return &PostService{
		appConfig:          appConfig,
		postRepository:     postRepository,
		postFileRepository: postFileRepository,
		dbContext:          dbContext,
	}
}

type IPostService interface {
	Add(param models.AddPostParam, userId string) *models.ServiceResultModel
}

type PostService struct {
	appConfig          *models.AppConfigModel
	postRepository     *repositories.PostRepository
	postFileRepository *repositories.PostFileRepository
	dbContext          *context.DatabaseContext
}

func (service *PostService) Add(param models.AddPostParam, userId string) *models.ServiceResultModel {

	result := &models.ServiceResultModel{
		StatusCode:    "00",
		StatusMessage: "",
	}

	postId, _ := uuid.NewV4()

	tx := service.dbContext.Db.Begin()

	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	dataPostInsert := &entities.Post{
		UserID:  userId,
		Title:   param.Title,
		Content: param.Content,
	}

	dataPostInsert.BaseEntity.ID = postId.String()
	dataPostInsert.BaseEntity.CreatedDate = time.Now()
	dataPostInsert.BaseEntity.IsDeleted = false

	txError := tx.Table("Post").Create(dataPostInsert).Error

	if txError != nil {
		tx.Rollback()
		result.StatusCode = "13"
		result.StatusMessage = "Insert data failed"
		return result
	}

	listDataPostFileInsert := []*entities.PostFile{}

	//get all file

	for _, f := range param.Files {

		dataFile := &entities.PostFile{}

		file, err := f.Open()

		if err != nil {
			tx.Rollback()
			result.StatusCode = "10"
			result.StatusMessage = "Open file failed"
			return result
		}

		//create destination file making sure the path is writeable.
		chunkFileName := strings.Split(f.Filename, ".")
		fileExt := chunkFileName[(len(chunkFileName) - 1)]
		newFileName := fmt.Sprintf("%s__%s__%d.%s", userId, dataPostInsert.BaseEntity.ID, time.Now().UnixNano(), fileExt)
		dst, err := os.Create(service.appConfig.PostFileDirPath + newFileName)

		dataFile.FileName = newFileName
		dataFile.FileSize = f.Size
		dataFile.FileType = fileExt
		dataFile.PostID = dataPostInsert.BaseEntity.ID

		if err != nil {
			tx.Rollback()
			result.StatusCode = "11"
			result.StatusMessage = "Save file failed"
			return result
		}

		defer dst.Close()

		if _, err := io.Copy(dst, file); err != nil {
			tx.Rollback()
			result.StatusCode = "12"
			result.StatusMessage = "Save file failed"
			return result
		}

		listDataPostFileInsert = append(listDataPostFileInsert, dataFile)
	}

	txError = service.postFileRepository.CreateRangeTransaction(tx, listDataPostFileInsert)

	if txError != nil {
		tx.Rollback()
		result.StatusCode = "13"
		result.StatusMessage = "Insert data failed"
		return result
	}

	tx.Commit()

	return result
}
