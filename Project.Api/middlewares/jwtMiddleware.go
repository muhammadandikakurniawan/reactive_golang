package middlewares

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	"project.helper/helpers"
	"project.model/models"
	"project.service/services"
)

func InitJwtAuthMiddleware(userService *services.UserService, appConfig *models.AppConfigModel) *JwtAuthMiddleware {
	return &JwtAuthMiddleware{
		userService: userService,
		appConfig:   appConfig,
	}
}

type JwtAuthMiddleware struct {
	userService *services.UserService
	appConfig   *models.AppConfigModel
}

//JwtAuthMiddleware jwt auth
func (middleware *JwtAuthMiddleware) JwtAuth(next http.Handler) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {

			headerToken := r.Header.Get("Authorization")
			tokenChunk := strings.Split(headerToken, " ")

			if len(tokenChunk) != 2 || tokenChunk[0] != "Barier" {
				w.WriteHeader(http.StatusForbidden)
				return
			}

			tokenStr := tokenChunk[1]

			claims, err := helpers.VerifyToken(tokenStr, middleware.appConfig.JwtConfig.SecretKey)

			if err != nil {
				w.WriteHeader(http.StatusForbidden)
				return
			}

			userId := claims["user_id"]
			if userId == nil {
				w.WriteHeader(http.StatusForbidden)
				return
			}

			dataUser, err := middleware.userService.GetUserById(fmt.Sprintf("%v", userId))

			if err != nil {
				w.WriteHeader(http.StatusForbidden)
				return
			}

			fmt.Println(dataUser)

			ctx := context.WithValue(context.Background(), "UserLoginInfo", claims)
			r = r.WithContext(ctx)
			next.ServeHTTP(w, r)

		})
}

//JwtAuthMiddleware jwt auth
func LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			fmt.Print("================== jwt uth middleware ================")
			next.ServeHTTP(w, r)

		})
}
