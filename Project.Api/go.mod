module project.api

go 1.16

replace project.model => ./../Project.Modules/model

replace project.repository => ./../Project.Modules/repository

replace project.service => ./../Project.Modules/service

replace project.helper => ./../Project.Modules/helper

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-openapi/spec v0.20.3 // indirect
	github.com/gorilla/context v1.1.1
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/schema v1.2.0
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/rs/cors v1.7.0
	github.com/swaggo/swag v1.7.0
	golang.org/x/net v0.0.0-20210316092652-d523dce5a7f4 // indirect
	golang.org/x/sys v0.0.0-20210320140829-1e4c9ba3b0c4 // indirect
	golang.org/x/tools v0.1.0 // indirect
	project.helper v0.0.1
	project.model v0.0.1
	project.repository v0.0.1
	project.service v0.0.1
)
