package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"project.api/middlewares"
	"project.model/models"
	"project.service/services"
)

func InitPostController(router *mux.Router, jwtMiddleware *middlewares.JwtAuthMiddleware, postService *services.PostService) *PostController {
	controller := &PostController{
		postService: postService,
	}

	subRouter := router.PathPrefix("/api/PostService").Subrouter()
	subRouter.Use(jwtMiddleware.JwtAuth)
	subRouter.HandleFunc("/Add", controller.Add).Methods("POST")

	return controller
}

type PostController struct {
	postService *services.PostService
}

func (controller *PostController) Add(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var decoder = schema.NewDecoder()

	ctx := r.Context().Value("UserLoginInfo")

	r.ParseMultipartForm(30 << 20)

	serviceParam := models.AddPostParam{}

	decoder.Decode(&serviceParam, r.Form)

	dataUserLogin := ctx.(jwt.MapClaims)

	userId := dataUserLogin["user_id"].(string)

	fmt.Print(dataUserLogin)
	fmt.Print(userId)

	files := r.MultipartForm.File["Files"]

	serviceParam.Files = files

	result := controller.postService.Add(serviceParam, userId)

	// params["Files"] = files

	// for i, _ := range files {
	// 	// file := files[i]

	// 	// fmt.Println(file.Filename)
	// 	// fmt.Println(file.Size)

	// 	file, err := files[i].Open()

	// 	if err != nil {
	// 		http.Error(w, err.Error(), http.StatusInternalServerError)
	// 		return
	// 	}

	// 	//create destination file making sure the path is writeable.
	// 	dst, err := os.Create("./" + files[i].Filename)
	// 	defer dst.Close()
	// 	if err != nil {
	// 		http.Error(w, err.Error(), http.StatusInternalServerError)
	// 		return
	// 	}

	// 	if _, err := io.Copy(dst, file); err != nil {
	// 		http.Error(w, err.Error(), http.StatusInternalServerError)
	// 		return
	// 	}
	// }

	// if err != nil {
	// 	result["StatusCode"] = "500"
	// 	result["Message"] = "error"

	// 	json.NewEncoder(w).Encode(result)
	// }

	json.NewEncoder(w).Encode(result)
}
