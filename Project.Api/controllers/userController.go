package controllers

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"project.model/models"
	"project.service/services"
)

func InitUserController(router *mux.Router, userService *services.UserService) *UserController {
	controller := &UserController{
		userService: userService,
	}
	subRouter := router.PathPrefix("/api/UserService").Subrouter()

	subRouter.HandleFunc("/Register", controller.Register).Methods("POST")
	subRouter.HandleFunc("/Login", controller.Login).Methods("POST")

	return controller
}

type UserController struct {
	userService *services.UserService
}

func (controller *UserController) Register(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var params models.UserRegisterModel

	json.NewDecoder(r.Body).Decode(&params)

	result := controller.userService.Register(params)

	json.NewEncoder(w).Encode(result)

}

func (controller *UserController) Login(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var params models.UserLoginParamModel

	json.NewDecoder(r.Body).Decode(&params)

	result := controller.userService.Login(params)

	json.NewEncoder(w).Encode(result)

}

// func (controller *UserController) Login(w http.ResponseWriter, r *http.Request) {
// 	w.Header().Set("Content-Type", "application/json")

// 	var params models.UserLoginModel

// 	json.NewDecoder(r.Body).Decode(&params)

// 	dataCtx := &models.DataUserLoginModel{
// 		ID:       "1234-5678-9101-1213",
// 		UserName: "testContextUserLogin",
// 		Email:    "userLogin@Email.com",
// 	}

// 	// ctx := context.WithValue(context.Background(), "userlogin", dataCtx)
// 	// r.WithContext(ctx)
// 	// fmt.Print(ctx)

// 	context.Set(r, "userlogin", dataCtx)

// 	result := controller.userService.Login(params, r)

// 	json.NewEncoder(w).Encode(result)

// }
