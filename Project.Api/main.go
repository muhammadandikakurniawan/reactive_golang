package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"project.api/controllers"
	"project.api/middlewares"
	"project.helper/helpers"
	"project.repository/context"
	"project.repository/repositories"
	"project.service/services"
)

var (

	//app configuration
	appConfig             = helpers.LoadConfiguration("appSetting.json")
	dbRestGoProjectConfig = appConfig.DataBaseConfig.RestGoProject

	//repository
	dbContext          = context.InitDatabaseContext(dbRestGoProjectConfig.Username, dbRestGoProjectConfig.Password, dbRestGoProjectConfig.DbHost, dbRestGoProjectConfig.DbPort, dbRestGoProjectConfig.DbName)
	userRepository     = repositories.InitUserRepository(dbContext)
	postRepository     = repositories.InitPostRepository(dbContext)
	postFileRepository = repositories.InitPostFileRepository(dbContext)

	//service
	userService = services.InitUserService(userRepository, appConfig)
	postService = services.InitPostService(appConfig, postRepository, postFileRepository, dbContext)

	//middleware
	JwtMiddleware = middlewares.InitJwtAuthMiddleware(userService, appConfig)
)

func main() {

	router := mux.NewRouter()

	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./staticFiles/"))))

	//Controllers
	controllers.InitUserController(router, userService)
	controllers.InitPostController(router, JwtMiddleware, postService)

	cors := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
	})

	handler := cors.Handler(router)

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", appConfig.ServerConfig.Port), handler))
}
